<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::post('/withdrawmoney', 'Api\AccountController@withdrawMoney');
Route::post('/makedeposit', 'Api\AccountController@depositMoney');

Route::apiResources([
    'user' => 'UserController',
    'type' => 'TypeController',
    'account' => 'AccountController'
    ]);

Route::get('transactions', 'TransactionController@index');