<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class depositMoneyTest extends TestCase
{
    /**
     * A basic unit test deposit money.
     *
     * @return void
     */
    public function testDepositMoney()
    {
        $response = $this->json('POST', '/api/makedeposit', [
            'user_id' => '1',
            'amount' => '100',
            'acount_id' => '1'
        ]);

        $response->assertStatus(200)->assertJson([
            'status' => 200,
        ]);
    }
}
