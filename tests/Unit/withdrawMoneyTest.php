<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class withdrawMoneyTest extends TestCase
{
    /**
     * A basic unit test withdraw money..
     *
     * @return void
     */
    public function testWithdrawMoney()
    {
        $response = $this->json('POST', '/api/withdrawmoney', [
            'user_id' => '1',
            'amount' => '10',
            'acount_id' => '1'
        ]);

        $response->assertStatus(200)->assertJson([
                'status' => 200,
            ]);
    }
}
