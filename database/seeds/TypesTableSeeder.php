<?php

use Illuminate\Database\Seeder;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $type = new \App\Type();
        $type->name = 'Credito';
        $type->commission = 0.10;
        $type->save();

        $type = new \App\Type();
        $type->name = 'Debito';
        $type->save();
    }
}
