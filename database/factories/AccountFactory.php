<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Account;
use Faker\Generator as Faker;

$factory->define(Account::class, function (Faker $faker) {
    $type = random_int(1, 2);
    $topCredit = $type == 1 ? random_int(1, 50) * 100 : null;
    $available = $type == 1 ? random_int(0, $topCredit) : random_int(0, 100) * 100;
    return [
        'type_id' => $type,
        'top_credit' => $topCredit,
        'available_money' => $available
    ];
});
