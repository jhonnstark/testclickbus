<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'top_credit', 'type_id', 'available_money', 'user_id'
    ];

    public function type() {
        return $this->belongsTo(Type::class);
    }
}
