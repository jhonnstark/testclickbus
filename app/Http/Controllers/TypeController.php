<?php

namespace App\Http\Controllers;

use App\Http\Requests\TypeRequest;
use App\Http\Resources\TypeCollection;
use App\Http\Resources\TypeResource;
use App\Type;
use Illuminate\Http\Request;

class TypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return TypeCollection
     */
    public function index(): TypeCollection
    {
        return new TypeCollection(Type::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  TypeRequest $request
     *
     * @return TypeResource
     */
    public function store(TypeRequest $request): TypeResource
    {
        $type = Type::create($request->validated());
        return new TypeResource($type);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Type  $type
     * @return TypeResource
     */
    public function show(Type $type): TypeResource
    {
        return new TypeResource($type);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  TypeRequest  $request
     * @param  \App\Type  $type
     * @return TypeResource
     */
    public function update(TypeRequest $request, Type $type): TypeResource
    {
        return new TypeResource($type);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function destroy(Type $type)
    {
        $type->delete();
        return response()->json([
            'status' => 200,
            'message' => 'Type deleted.'
        ]);
    }
}
