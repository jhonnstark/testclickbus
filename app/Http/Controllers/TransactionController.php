<?php

namespace App\Http\Controllers;

use App\Http\Resources\TransactionCollection;
use App\Transaction;

class TransactionController extends Controller
{

    /**
     * Display a listing of the transaction.
     *
     * @return TransactionCollection
     */
    public function index(): TransactionCollection
    {
        return new TransactionCollection(Transaction::all());
    }
}
