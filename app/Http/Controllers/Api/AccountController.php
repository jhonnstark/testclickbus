<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\DepositMoneyRequest;
use App\Http\Requests\WithdrawMoneyRequest;
use Illuminate\Http\JsonResponse;
use App\Transaction;
use App\User;

class AccountController extends Controller
{

    /**
     * commissionCalc
     *
     * @param  float $amount
     * @param  float $commission
     *
     * @return float
     */
    private function commissionCalc($amount, $commission): float
    {
        return $amount * (1 + $commission);
    }
    
    public function withdrawMoney(WithdrawMoneyRequest $request)
    {
        $account_id = $request->acount_id;
        $user = User::where('id', $request->user_id)->with(['accounts' => function ($query) use ($account_id) {
            $query->where('id', $account_id);
        }, 'accounts.type'])->first();

        $account = $user->accounts->first();
        $total = $this->commissionCalc($request->amount, $account->type->commission);

        if ($account->available_money >= $total) {
            $transaction = new Transaction();
            $transaction->amount = - (int) $request->amount;
            $transaction->account_id = $account_id;
            $transaction->operation = 'Withdraw money';
            $transaction->save();
            $account->available_money -= $total;
            $account->save();
        } else {
            return response()->json([
                'status'  => 402,
                'data'    => $user,
                'message' => 'Money extraction unavailable.'
            ], 402);
        }
        
        return response()->json([
            'status'  => 200,
            'data'    => $user,
            'message' => 'Money extracted.'
        ]);
    }


    /**
     * Function that increses the money in the account
     *
     * @param  DepositMoneyRequest $request
     *
     * @return JsonResponse
     */
    public function depositMoney(DepositMoneyRequest $request): JsonResponse
    {
        $account_id = $request->acount_id;
        $user = User::where('id', $request->user_id)->with(['accounts' => function ($query) use ($account_id) {
            $query->where('id', $account_id);
        }])->first();

        $account = $user->accounts->first();

        if (!empty($account)) {
            $transaction = new Transaction();
            $transaction->amount = (int) $request->amount;
            $transaction->account_id = $account_id;
            $transaction->operation = 'Deposit money';
            $transaction->save();
            $account->available_money += $request->amount;
            $account->save();
        } else {
            return response()->json([
                'status'  => 402,
                'data'    => $user,
                'message' => 'Money insertion unavailable.'
            ], 402);
        }

        return response()->json([
            'status' => 200,
            'data'    => $user,
            'message' => 'Money deposited.'
        ]);
    }
}
