<?php

namespace App\Http\Controllers;

use App\Account;
use App\Http\Requests\AccountRequest;
use App\Http\Resources\AccountCollection;
use App\Http\Resources\AccountResource;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return AccountCollection
     */
    public function index()
    {
        return new AccountCollection(Account::all()->load('type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  AccountRequest  $request
     * @return AccountResource
     */
    public function store(AccountRequest $request)
    {
        $valid = $request->validated();
        if ( $valid['type_id'] !== 2) {
            $valid['top_credit'] = $request->available_money;
        }
        $account = Account::create($valid);
        return new AccountResource($account->load('type'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Account  $account
     * @return AccountResource
     */
    public function show(Account $account)
    {
        return new AccountResource($account->load('type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  AccountRequest  $request
     * @param  \App\Account  $account
     * @return AccountResource
     */
    public function update(AccountRequest $request, Account $account)
    {
        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function destroy(Account $account)
    {
        $account->delete();
        return response()->json([
            'status' => 200,
            'message' => 'Acount deleted.'
        ]);
    }
}
