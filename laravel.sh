#!/bin/sh
# Note: this will run after the image is up

composer install --no-dev
php -r "file_exists('.env') || copy('.env.example', '.env');"
php artisan key:generate --ansi
php artisan serve --host=0.0.0.0 --port=3000