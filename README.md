# ClickBus Backend Test

##  Description

This is a test for clickbus requitment team for the Tech Department.

### Cash Machine

* Create a model to represent a User with the following use cases
* A user has 'N' accounts
* An account can be credit or debit type
* An account has 'X' credit or available amount 
* A user can withdraw from the credit account with a 10% commission cost, only if the user has enough credit  
* A user can pay only for his credit account 
* A user can withdraw from the debit account only if the user has enough amount  
* A user can deposit more in his debit account 
#### Note: feel free to design the Model and attributes

## We recommend
* PHP >= 7.1 (Node or Python are options too)
* Database MySQL or MariaDB (Any other SQL or No SQL database are option too) 
* A modern MVC Framework
* File README.md with instructions/explanations on how to execute and test the project

### Differential
* RestFul API
* Unit tests 
* Docker

### Evaluation criteria
* Creativity: The previous instructions do not limit any desire of the developer, be free
* Organization: project structure, versioning
* Good practices, Standards (PSRs, Linteners, etc)
* Technology: use of paradigms, frameworks and libraries
* Design patterns, OOP, S.O.L.I.D. principles

## Prerequisites
* Docker for MacOs
* Postman or a similar app to make the request since there's no UI,

## Installing
The proyect is ready to run so you only need to type in the root of the project

```
docker-compose up --build
```

## Api Documentation
The documentation is located in a postman link. [click here](https://documenter.getpostman.com/view/5600080/SVmqyzsV)

## Running the tests

The proyect uses [Phpunit](https://phpunit.de/) (v8+) for testing included with composer. 

```
vender/phpunit/phpunit/phpunit
```
o de forma global 

```
phpunit
```

## Built With

* [Laravel](https://laravel.com/) - The php framework used to develop the proyect
* [Docker](https://www.docker.com/) - Application to build container applications


## Versioning

The version control implemented was done with [Git](https://git-scm.com/).

## Authors

* **Jonathan Cervantes** - [jhonncerv@gmail.com](https://bitbucket.org/%7Bc0df598d-dc7d-44fc-8f53-00152a545822%7D/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details